const std = @import("std");
const w4 = @import("wasm4.zig");

const smiley = [8]u8{
    0b11000011,
    0b10000001,
    0b00100100,
    0b00100100,
    0b00000000,
    0b00100100,
    0b10011001,
    0b11000011,
};

var time: u32 = 0;

const log_semitone: f32 = @log(2.0) / 12.0;

fn note_freq(note: f32) u16 {
    const freq = 440.0 * @exp((note - 69.0) * log_semitone);
    return @intFromFloat(@round(freq));
}

export fn start() void {
    time = 0;
    w4.PALETTE[0] = 0x000000;
    w4.PALETTE[1] = 0x404040;
    w4.PALETTE[2] = 0x808080;
    w4.PALETTE[3] = 0xFFFFFF;
    // w4.PALETTE[0] = 0x000000;
    // w4.PALETTE[1] = 0x3B3B3B;
    // w4.PALETTE[2] = 0x777777;
    // w4.PALETTE[3] = 0xB9B9B9;

    w4.trace("Hello welcome to the game!");
}

const u32x4 = @Vector(4, u32);
const f32x4 = @Vector(4, f32);
const u8x16 = @Vector(16, u8);
const boolx4 = @Vector(4, bool);
const f32x16 = @Vector(16, f32);

const Vec4 = f32x4;
fn vec_adjust(v: Vec4) Vec4 {
    return v / @as(Vec4, @splat(v[3]));
}
fn vec_length(v: Vec4) f32 {
    const v2 = v * v;
    return @sqrt(v2[0] + v2[1] + v2[2]);
}
fn vec_normalize(v: Vec4) Vec4 {
    const len = vec_length(v);
    return v / Vec4{ len, len, len, 1 };
}
fn vec_add(a: Vec4, b: Vec4) Vec4 {
    return Vec4{ a[0] + b[0], a[1] + b[1], a[2] + b[2], 1 };
}
fn vec_dot(a: Vec4, b: Vec4) f32 {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}
// const Tri = [3]usize;

const Rotor3 = f32x4;
const Rotor3_ident = rotor_from_to(Vec4{ 1, 0, 0, 1 }, Vec4{ 1, 0, 0, 1 });

fn rotor_from_to(from_dir_v: Vec4, to_dir_v: Vec4) Rotor3 {
    const to_dir = vec_normalize(to_dir_v);
    const from_dir = vec_normalize(from_dir_v);
    const halfway = vec_normalize(vec_add(to_dir, from_dir));

    return Rotor3{
        vec_dot(halfway, from_dir),
        halfway[0] * from_dir[1] - halfway[1] * from_dir[0],
        halfway[1] * from_dir[2] - halfway[2] * from_dir[1],
        halfway[2] * from_dir[0] - halfway[0] * from_dir[2],
    };
}

fn rotor_combine(lhs: Rotor3, rhs: Rotor3) Rotor3 {
    return Rotor3{
        lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] - lhs[3] * rhs[3],
        lhs[0] * rhs[1] + lhs[1] * rhs[0] - lhs[2] * rhs[3] + lhs[3] * rhs[2],
        lhs[0] * rhs[2] + lhs[1] * rhs[3] + lhs[2] * rhs[0] - lhs[3] * rhs[1],
        lhs[0] * rhs[3] - lhs[1] * rhs[2] + lhs[2] * rhs[1] + lhs[3] * rhs[0],
    };
}

fn rotor_reverse(r: Rotor3) Rotor3 {
    return r * Rotor3{ 1, -1, -1, -1 };
}

fn rotor_transform(r: Rotor3, v: Vec4) Vec4 {
    const S = Vec4{
        r[0] * v[0] + r[1] * v[1] - r[3] * v[2],
        r[0] * v[1] - r[1] * v[0] + r[2] * v[2],
        r[0] * v[2] - r[2] * v[1] + r[3] * v[0],
        r[1] * v[2] + r[2] * v[0] + r[3] * v[1],
    };

    return Vec4{
        S[0] * r[0] + S[1] * r[1] + S[3] * r[2] - S[2] * r[3],
        S[1] * r[0] - S[0] * r[1] + S[2] * r[2] + S[3] * r[3],
        S[2] * r[0] + S[3] * r[1] - S[1] * r[2] + S[0] * r[3],
        1,
    };
}

fn mat_rotor(r: Rotor3) Mat4 {
    const X = rotor_transform(r, Vec4{ 1, 0, 0, 1 });
    const Y = rotor_transform(r, Vec4{ 0, 1, 0, 1 });
    const Z = rotor_transform(r, Vec4{ 0, 0, 1, 1 });
    return Mat4{
        X[0], Y[0], Z[0], 0,
        X[1], Y[1], Z[1], 0,
        X[2], Y[2], Z[2], 0,
        0,    0,    0,    1,
    };
}

const verticies = [_]Vec4{
    Vec4{ 0, 1, 0, 1 },
    Vec4{ 1, 0, 0, 1 },
    Vec4{ 0, 0, 1, 1 },
    Vec4{ 1, 1, 1, 1 },
};

const tris = [_][]const usize{
    &[_]usize{ 0, 1, 2 },
    &[_]usize{ 0, 2, 3 },
    &[_]usize{ 0, 1, 3 },
    &[_]usize{ 1, 2, 3 },
};

// const Mat4 = [4]f32x4;
// const Mat4_ident = [4]f32x4{ f32x4{ 1, 0, 0, 0 }, f32x4{ 0, 1, 0, 0 }, f32x4{ 0, 0, 1, 0 }, f32x4{ 0, 0, 0, 1 } };

// fn mat_mul(mat: Mat4, vec: Vec4) Vec4 {
//     return mat[0] * @as(Vec4, @splat(vec[0])) + mat[1] * @as(Vec4, @splat(vec[1])) + mat[2] * @as(Vec4, @splat(vec[2])) + mat[3] * @as(Vec4, @splat(vec[3]));
// }

const Mat4 = @Vector(16, f32);
const Mat4_ident = Mat4{
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1,
};
fn mat_mul(mat: Mat4, vec: Vec4) Vec4 {
    return std.simd.extract(mat, 0, 4) * @as(Vec4, @splat(vec[0])) + std.simd.extract(mat, 4, 4) * @as(Vec4, @splat(vec[1])) + std.simd.extract(mat, 8, 4) * @as(Vec4, @splat(vec[2])) + std.simd.extract(mat, 12, 4) * @as(Vec4, @splat(vec[3]));
}
fn mat_transpose(mat: Mat4) Mat4 {
    return Mat4{
        mat[0], mat[4], mat[8],  mat[12],
        mat[1], mat[5], mat[9],  mat[13],
        mat[2], mat[6], mat[10], mat[14],
        mat[3], mat[7], mat[11], mat[15],
    };
}

inline fn mat_mul_mat_helper(comptime o: comptime_int, m: Mat4) Mat4 {
    return Mat4{
        m[0 + o],  m[0 + o],  m[0 + o],  m[0 + o],
        m[4 + o],  m[4 + o],  m[4 + o],  m[4 + o],
        m[8 + o],  m[8 + o],  m[8 + o],  m[8 + o],
        m[12 + o], m[12 + o], m[12 + o], m[12 + o],
    };
}

fn mat_mul_mat(a: Mat4, b: Mat4) Mat4 {
    const bt = mat_transpose(b);
    return mat_mul_mat_helper(0, a) * mat_transpose(mat_mul_mat_helper(0, bt)) + mat_mul_mat_helper(1, a) * mat_transpose(mat_mul_mat_helper(1, bt)) + mat_mul_mat_helper(2, a) * mat_transpose(mat_mul_mat_helper(2, bt)) + mat_mul_mat_helper(3, a) * mat_transpose(mat_mul_mat_helper(3, bt));
}

fn mat_mul_mats(ms: anytype) Mat4 {
    var res = Mat4_ident;
    inline for (ms) |m| {
        res = mat_mul_mat(res, m);
    }
    return res;
}

fn mat_scale(s: f32) Mat4 {
    return Mat4{
        s, 0, 0, 0,
        0, s, 0, 0,
        0, 0, s, 0,
        0, 0, 0, 1,
    };
}

fn mat_rotY(a: f32) Mat4 {
    return Mat4{
        @cos(a), 0, -@sin(a), 0,
        0,       1, 0,        0,
        @sin(a), 0, @cos(a),  0,
        0,       0, 0,        1,
    };
}

fn mat_translate(v: Vec4) Mat4 {
    const v_adj = vec_adjust(v);
    return Mat4{
        1,        0,        0,        0,
        0,        1,        0,        0,
        0,        0,        1,        0,
        v_adj[0], v_adj[1], v_adj[2], 1,
    };
}

fn draw_poly(polygon: []const usize, trans: Mat4) void {
    for (polygon, 0..) |_, i| {
        const j = (i + 1) % polygon.len;

        const pi = verticies[polygon[i]];
        const pj = verticies[polygon[j]];

        const tpi = vec_adjust(mat_mul(trans, pi));
        const tpj = vec_adjust(mat_mul(trans, pj));
        w4.line(@intFromFloat(tpi[0]), @intFromFloat(tpi[1]), @intFromFloat(tpj[0]), @intFromFloat(tpj[1]));
    }
}

var rotation = Rotor3_ident;
const rotate_left = rotor_from_to(Vec4{ 0, 0, 1, 1 }, Vec4{ -1, 0, 30, 1 });
const rotate_right = rotor_from_to(Vec4{ 0, 0, 1, 1 }, Vec4{ 1, 0, 30, 1 });
const rotate_up = rotor_from_to(Vec4{ 0, 0, 1, 1 }, Vec4{ 0, -1, 30, 1 });
const rotate_down = rotor_from_to(Vec4{ 0, 0, 1, 1 }, Vec4{ 0, 1, 30, 1 });

fn pixel(x: f32, y: f32, t: f32) f32 {
    // return (x * 1.5 + y * 2.0) / 30.0 - t / 4.0 - 5.0;
    // return (x / 200.0 + y / 300.0 + 0.0 * t / 32.0) / 1.0 - 0.15;

    const a = t / 8.0;
    const x1 = x / 80.0 - 1.0;
    const y1 = y / 80.0 - 1.0;

    const x2 = x1 * @cos(a) - y1 * @sin(a);
    const y2 = x1 * @sin(a) + y1 * @cos(a);
    _ = y2;
    return (x2 * 0.5 + 0.5);
}

// fn map_color(z: f32) u8 {
//     return if (z < 0.25) 0 else if (z < 0.5) 1 else if (z < 0.75) 2 else 3;
// }

fn dither_map_color(z: @Vector(16, f32), yi: u8) u8x16 {
    const unclamped_color_value = z * @as(@Vector(16, f32), @splat(3.0));
    const color_value = @max(@min(unclamped_color_value, @as(@Vector(16, f32), @splat(2.9999998))), @as(@Vector(16, f32), @splat(0.0)));

    const color_fract_value = @mod(color_value, @as(@Vector(16, f32), @splat(1.0)));

    const color_range_f = @ceil(color_value);
    var color_range: u8x16 = undefined;
    inline for (0..16) |i| {
        color_range[i] = @intFromFloat(color_range_f[i]);
    }

    // var color_range: u8x16 = @splat(3);
    // color_range = @select(u8, color_value <= @as(@Vector(16, f32), @splat(2.0)), @as(u8x16, @splat(2)), color_range);
    // color_range = @select(u8, color_value <= @as(@Vector(16, f32), @splat(1.0)), @as(u8x16, @splat(1)), color_range);
    // color_range = @select(u8, color_value <= @as(@Vector(16, f32), @splat(0.0)), @as(u8x16, @splat(0)), color_range);

    const upper_color = color_range;
    const lower_color = upper_color -| @as(u8x16, @splat(1));

    const cv = @floor(color_fract_value * @as(@Vector(16, f32), @splat(256.0)));
    var f: u8x16 = undefined;
    inline for (0..16) |i| {
        f[i] = @intFromFloat(cv[i]);
    }

    const threshold: u8x16 = vac_matrix[yi % 16];
    const t = f < threshold;
    return @select(u8, t, lower_color, upper_color);
}

fn merge_colors(c: u8x16) u32 {
    const s1 = c << u8x16{ 0, 2, 4, 6, 0, 2, 4, 6, 0, 2, 4, 6, 0, 2, 4, 6 };
    const m1 = @Vector(8, u8){ s1[0], s1[2], s1[4], s1[6], s1[8], s1[10], s1[12], s1[14] } | @Vector(8, u8){ s1[1], s1[3], s1[5], s1[7], s1[9], s1[11], s1[13], s1[15] };
    const m2 = @Vector(4, u8){ m1[0], m1[2], m1[4], m1[6] } | @Vector(4, u8){ m1[1], m1[3], m1[5], m1[7] };
    return @bitCast(m2);
}

const bayer_matrix = [16][16]u8{
    [_]u8{ 0, 128, 32, 160, 8, 136, 40, 168, 2, 130, 34, 162, 10, 138, 42, 170 },
    [_]u8{ 192, 64, 224, 96, 200, 72, 232, 104, 194, 66, 226, 98, 202, 74, 234, 106 },
    [_]u8{ 48, 176, 16, 144, 56, 184, 24, 152, 50, 178, 18, 146, 58, 186, 26, 154 },
    [_]u8{ 240, 112, 208, 80, 248, 120, 216, 88, 242, 114, 210, 82, 250, 122, 218, 90 },
    [_]u8{ 12, 140, 44, 172, 4, 132, 36, 164, 14, 142, 46, 174, 6, 134, 38, 166 },
    [_]u8{ 204, 76, 236, 108, 196, 68, 228, 100, 206, 78, 238, 110, 198, 70, 230, 102 },
    [_]u8{ 60, 188, 28, 156, 52, 180, 20, 148, 62, 190, 30, 158, 54, 182, 22, 150 },
    [_]u8{ 252, 124, 220, 92, 244, 116, 212, 84, 254, 126, 222, 94, 246, 118, 214, 86 },
    [_]u8{ 3, 131, 35, 163, 11, 139, 43, 171, 1, 129, 33, 161, 9, 137, 41, 169 },
    [_]u8{ 195, 67, 227, 99, 203, 75, 235, 107, 193, 65, 225, 97, 201, 73, 233, 105 },
    [_]u8{ 51, 179, 19, 147, 59, 187, 27, 155, 49, 177, 17, 145, 57, 185, 25, 153 },
    [_]u8{ 243, 115, 211, 83, 251, 123, 219, 91, 241, 113, 209, 81, 249, 121, 217, 89 },
    [_]u8{ 15, 143, 47, 175, 7, 135, 39, 167, 13, 141, 45, 173, 5, 133, 37, 165 },
    [_]u8{ 207, 79, 239, 111, 199, 71, 231, 103, 205, 77, 237, 109, 197, 69, 229, 101 },
    [_]u8{ 63, 191, 31, 159, 55, 183, 23, 151, 61, 189, 29, 157, 53, 181, 21, 149 },
    [_]u8{ 255, 127, 223, 95, 247, 119, 215, 87, 253, 125, 221, 93, 245, 117, 213, 85 },
};

const vac_matrix = [16][16]u8{
    [_]u8{ 106, 66, 6, 125, 101, 45, 121, 64, 142, 84, 253, 103, 214, 50, 172, 17 },
    [_]u8{ 43, 203, 171, 225, 73, 153, 243, 169, 109, 194, 21, 67, 141, 199, 76, 221 },
    [_]u8{ 129, 254, 87, 25, 192, 212, 3, 35, 234, 47, 156, 237, 26, 118, 94, 159 },
    [_]u8{ 32, 58, 148, 117, 53, 131, 97, 181, 78, 116, 207, 89, 174, 226, 2, 195 },
    [_]u8{ 108, 165, 233, 10, 177, 248, 61, 222, 139, 189, 9, 132, 37, 151, 62, 240 },
    [_]u8{ 15, 186, 96, 209, 79, 146, 16, 158, 30, 55, 218, 75, 252, 187, 85, 137 },
    [_]u8{ 220, 70, 46, 128, 28, 231, 114, 204, 99, 239, 168, 102, 48, 112, 22, 201 },
    [_]u8{ 40, 143, 250, 200, 163, 91, 44, 175, 69, 127, 24, 145, 205, 230, 164, 122 },
    [_]u8{ 80, 160, 104, 4, 63, 217, 136, 251, 0, 213, 86, 179, 8, 72, 33, 244 },
    [_]u8{ 14, 223, 52, 178, 119, 191, 20, 81, 154, 188, 41, 246, 120, 150, 98, 182 },
    [_]u8{ 134, 196, 88, 241, 147, 34, 235, 111, 54, 227, 105, 60, 210, 173, 49, 215 },
    [_]u8{ 65, 38, 124, 12, 71, 100, 170, 198, 135, 11, 166, 140, 18, 83, 236, 110 },
    [_]u8{ 167, 255, 152, 184, 229, 211, 42, 74, 249, 31, 90, 193, 242, 36, 138, 5 },
    [_]u8{ 77, 216, 23, 107, 57, 133, 1, 155, 115, 176, 219, 68, 113, 161, 190, 206 },
    [_]u8{ 123, 92, 51, 197, 162, 82, 238, 95, 202, 56, 130, 39, 228, 93, 59, 29 },
    [_]u8{ 232, 185, 144, 245, 19, 208, 180, 27, 224, 13, 157, 183, 7, 126, 247, 149 },
};

export fn update() void {
    // Update time
    time += 1;
    const time_secs: f32 = @as(f32, @floatFromInt(time)) / 60.0;

    // Update rotation
    if (w4.GAMEPAD1.left) {
        rotation = rotor_combine(rotation, rotate_left);
    }
    if (w4.GAMEPAD1.right) {
        rotation = rotor_combine(rotation, rotate_right);
    }
    if (w4.GAMEPAD1.up) {
        rotation = rotor_combine(rotation, rotate_up);
    }
    if (w4.GAMEPAD1.down) {
        rotation = rotor_combine(rotation, rotate_down);
    }

    // Draw background
    for (0..160) |yi| {
        for (0..10) |xc| {
            const y: @Vector(16, f32) = @splat(@floatFromInt(yi));
            const x: @Vector(16, f32) = std.simd.iota(f32, 16) + @as(@Vector(16, f32), @splat(@floatFromInt(xc * 16)));

            var z: @Vector(16, f32) = undefined;
            inline for (0..16) |i| {
                z[i] = pixel(x[i], y[i], time_secs);
            }

            var c: u8x16 = dither_map_color(z, @truncate(yi));

            w4.FRAMEBUFFER_U32[xc + yi * 10] = merge_colors(c);
            // std.mem.writeIntNative(u32, w4.FRAMEBUFFER + ind, combined_colors);
        }
    }

    // Draw 3d geometry
    const view_matrix = mat_mul_mats(.{
        mat_translate(Vec4{ -0.5, -0.5, -0.5, 1 }),
        mat_scale(20.0),
        mat_rotor(rotation),
        mat_translate(Vec4{ 80, 80, 0, 1 }),
    });

    w4.DRAW_COLORS.draw_color_1 = 4;
    for (tris) |poly| {
        draw_poly(poly, view_matrix);
    }

    // // Draw text
    // w4.DRAW_COLORS.draw_color_1 = 2;
    // w4.text("\x86\x87\x84\x85 to rotate", 10, 10);

    // if (w4.GAMEPAD1.x) {
    //     w4.DRAW_COLORS.draw_color_1 = 4;
    // }

    // w4.blit(&smiley, 76, 86, 8, 8, w4.BLIT_1BPP);
    // w4.text("Press \x80 to blink", 16, 100);
}
