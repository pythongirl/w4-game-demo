const std = @import("std");

pub fn build(b: *std.Build) !void {
    const optimize = b.standardOptimizeOption(.{});

    const wasm_features = std.Target.wasm.featureSet(&[_]std.Target.wasm.Feature{ std.Target.wasm.Feature.bulk_memory, std.Target.wasm.Feature.simd128, std.Target.wasm.Feature.nontrapping_fptoint });

    const lib = b.addSharedLibrary(.{
        .name = "cart",
        .root_source_file = .{ .path = "src/main.zig" },
        .optimize = optimize,
        .target = .{ .cpu_arch = .wasm32, .os_tag = .freestanding, .cpu_features_add = wasm_features },
    });

    lib.import_memory = true;
    lib.initial_memory = 65536;
    lib.max_memory = 65536;
    lib.stack_size = 14752;
    lib.strip = true;

    // Export WASM-4 symbols
    lib.export_symbol_names = &[_][]const u8{ "start", "update" };

    // b.default_step.dependOn(&lib.step);
    b.installArtifact(lib);
}
